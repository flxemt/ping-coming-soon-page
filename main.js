const form = document.getElementById('form')
const emailInput = document.getElementById('email-input')

function handleFormSubmit(event) {
  event.preventDefault()
  if (!emailInput.value.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)) {
    emailInput.classList.add('input-error')
  } else {
    emailInput.classList.remove('input-error')
  }
}

form.addEventListener('submit', handleFormSubmit)
